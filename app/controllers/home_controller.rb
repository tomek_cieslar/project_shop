class HomeController < ApplicationController
  def index
    @news =  News.where(public: true).last(5)
    @categories = Category.all
  end
end
