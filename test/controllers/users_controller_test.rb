require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  setup do
    @user = users(:user_tomek)
  end


  test "user can logg in" do
    sign_in @user
    get user_path(@user.id)
    assert_response :success
  end
  
  test "user cant see his profile if user in not logged in" do
    get user_path(@user.id)
    assert_response :redirect
  end
end
