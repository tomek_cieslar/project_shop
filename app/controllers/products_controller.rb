class ProductsController < ApplicationController
  def index
    search_query = products_by_search_query(params[:search])
    category = params[:category_id]
    # using string from products_by_search_query to create complete query
    if category.present?
      @products = Product.where(category_id: category).where(search_query).joins(:category, :manufacturer)
    else
      @products = Product.where(search_query).joins(:category, :manufacturer)
    end
  end

  def show
    @product = Product.find(params[:id])
    @cart_item = CartItem.new
  end

  def edit
    @product = Product(params[:id])
  end

  def new
    @product = Product.new
  end

  def create
    @product = Product.new(product_params)
    @product.save
    redirect_to products_path
  end

  def update
    @product = Product.find(params[:id])
  	@product.update(product_params)
    @product.save
    flash[:success] = "Status has been updated"
    redirect_to products_path
  end

  private

  #creating part of sql query using params
  def products_by_search_query(search)
    if search.present?
      search_string =  '%' + search + '%'
      search_query = '"categories"."category_name" ILIKE :search_string OR
      "manufacturers"."manufacturer_name" ILIKE :search_string OR
       "products"."name" ILIKE :search_string',
      {search_string: search_string }
    end
    search_query
  end

  def product_params
    params.require(:product).permit(
      :name,
      :category_id,
      :manufacturer_id,
      :parameters,
      :description,
      :in_stock,
      :price,
      :package_specification,
      :product_code
    )
  end

end
