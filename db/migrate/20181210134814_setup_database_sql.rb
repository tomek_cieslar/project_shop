class SetupDatabaseSql < ActiveRecord::Migration[5.2]
  def up
    execute <<-SQL
    drop table if exists NEWS, MANUFACTURERS, CATEGORIES, STATUSES,
PRODUCTS, ORDER_ITEMS, ORDERS, ADDRESSES, CUSTOMERS, USERS, CARTS, CART_ITEMS,
SERIAL_NUMBERS cascade;
--drop view if exists Customer_View , Order_View, Product_View cascade;
--drop trigger if exists update_last_status on orders;
drop function if exists update_last_status_date();
--drop index if exists user_id_idx, user_email_idx,
--user_reset_password_token_idx, news_id_idx, customer_id_idx,
--address_id_idx, order_id_idx, product_id_idx, order_item_id_idx,
--status_id_idx, serial_number_item_id_idx, category_id_idx,
--manufacturer_id_idx, cart_id_idx, cart_item_id_idx;

-- Create table

create table NEWS(
ID serial primary key,
CREATED_DATE timestamp not null,
LAST_EDIT_DATE timestamp not null,
TITLE varchar(200) not null,
CONTENT varchar(1000) not null,
thumbnail json,
link varchar(200),
public boolean not null
);

create table USERS(
ID serial primary key,
EMAIL varchar(30) unique not null,
ENCRYPTED_PASSWORD varchar(64) not null,
ACCESS_LEVEL int not null default 3,
CREATED_AT timestamp not null,
UPDATED_AT timestamp not null,
RESET_PASSWORD_TOKEN varchar(64),
RESET_PASSWORD_SENT_AT timestamp,
REMEMBER_CREATED_AT timestamp,
SIGN_IN_COUNT int not null default 0,
CURRENT_SIGN_IN_AT timestamp,
LAST_SIGN_IN_AT timestamp,
CURRENT_SIGN_IN_IP inet,
LAST_SIGN_IN_IP inet
);

create table CUSTOMERS(
ID serial primary key,
USER_ID int unique not null REFERENCES USERS(ID) ON DELETE RESTRICT,
FIRST_NAME varchar(20) not null,
LAST_NAME varchar(30) not null
);

create table ADDRESSES(
ID serial primary key,
  CUSTOMER_ID int not null REFERENCES CUSTOMERS(ID) ON DELETE RESTRICT,
  STREET varchar(30),
  HOUSE_NUMBER varchar(10) not null,
  APARTMENT_NUMBER varchar(10),
  POSTCODE varchar(6) not null,
  CITY varchar(30) not null
);

create table STATUSES(
ID serial primary key,
STATUS_NAME varchar(20)  NOT NULL
);

create table CATEGORIES(
ID serial primary key,
CATEGORY_NAME varchar(20) UNIQUE NOT NULL
);

create table MANUFACTURERS(
ID serial primary key,
MANUFACTURER_NAME varchar(20) UNIQUE NOT NULL
);

create table PRODUCTS(
ID serial primary key,
NAME varchar(50) not null,
CATEGORY_ID int not null REFERENCES CATEGORIES(ID) ON DELETE RESTRICT,
MANUFACTURER_ID int not null REFERENCES MANUFACTURERS(ID) ON DELETE RESTRICT,
PARAMETERS varchar(500),
DESCRIPTION text not null,
THUMBNAIL json,
IN_STOCK int not null,
PRICE numeric(7,2) not null,
PACKAGE_SPECIFICATION varchar(20) not null,
PRODUCT_CODE varchar(20) not null unique
);

create table ORDERS(
ID serial primary key,
CUSTOMER_ID int not null REFERENCES CUSTOMERS(ID) ON DELETE RESTRICT,
STATUS_ID int not null REFERENCES STATUSES(ID) ON DELETE RESTRICT,
LAST_STATUS_CHANGE timestamp not null,
ORDER_DATE date not null,
ADDRESS_ID int not null REFERENCES ADDRESSES(ID) ON DELETE RESTRICT,
DELIVERY_METHOD varchar(30) not null,
EXPECTED_ARRIVAL_TIME date not null,
NOTES varchar(1000)
);

create table ORDER_ITEMS(
ID serial primary key,
ORDER_ID int not null REFERENCES ORDERS(ID) ON DELETE RESTRICT,
ITEM_NAME varchar(50) not null,
PRICE numeric(7,2) not null,
PRODUCT_ID int REFERENCES PRODUCTS(ID) ON DELETE RESTRICT,
COUNT int not null,
SERIAL_NUMBER varchar(16)
);

create table SERIAL_NUMBERS(
ID serial primary key,
PRODUCT_ID int not null REFERENCES PRODUCTS(ID) ON DELETE RESTRICT,
SERIAL_NUMBER varchar(16) NOT NULL
);

create table CARTS(
    ID serial primary key,
    USER_ID int not null REFERENCES USERS(ID) ON DELETE RESTRICT
    );

    create table CART_ITEMS(
    ID serial primary key,
    CART_ID int not null REFERENCES CARTS(ID) ON DELETE RESTRICT,
    PRODUCT_ID int not null REFERENCES PRODUCTS(ID) ON DELETE RESTRICT,
    QUANTITY int not null
    );

--Indexes

CREATE UNIQUE INDEX user_id_idx ON users(id);
CREATE UNIQUE INDEX user_email_idx ON users(email);
CREATE UNIQUE INDEX user_reset_password_token_idx ON users(reset_password_token);
CREATE UNIQUE INDEX news_id_idx ON news(id);
CREATE UNIQUE INDEX customer_id_idx ON customers(id);
CREATE UNIQUE INDEX address_id_idx ON addresses(id);
CREATE UNIQUE INDEX order_id_idx ON orders(id);
CREATE UNIQUE INDEX product_id_idx ON products(id);
CREATE UNIQUE INDEX order_item_id_idx ON order_items(id);
CREATE UNIQUE INDEX status_id_idx ON statuses(id);
CREATE UNIQUE INDEX serial_number_item_id_idx ON serial_numbers(id);
CREATE UNIQUE INDEX category_id_idx ON categories(id);
CREATE UNIQUE INDEX manufacturer_id_idx ON manufacturers(id);
CREATE UNIQUE INDEX cart_id_idx ON carts(id);
CREATE UNIQUE INDEX cart_item_id_idx ON cart_items(id);

--Views

create view Customer_View as
select a.customer_id, c.first_name, c.last_name, a.street,
a.house_number, a.apartment_number, a.postcode, a.city
from Customers c
left join Addresses a on c.id = a.customer_id;

create view Order_View as
select oi.order_id, o.customer_id, oi.item_name, oi.price, oi.product_id,
oi.count, oi.serial_number, a.street, a.house_number,
a.apartment_number, a.postcode, a.city, o.last_status_change,
o.order_date, o.expected_arrival_time, o.notes
from order_items oi
left join Orders o on oi.order_id = o.id
left join Addresses a on o.address_id = a.id;

create view product_view as
select p.id, p.name, m.manufacturer_name, c.category_name,
p.parameters, p.description, p.thumbnail, p.in_stock, p.price,
p.package_specification, p.product_code, sn.serial_number
from products p
left join manufacturers m on p.manufacturer_id = m.id
left join categories c on p.category_id = c.id
left join serial_numbers sn on sn.product_id = p.id;

--Trigger function

CREATE FUNCTION update_last_status_date() RETURNS trigger AS $update_last_status_date$
  BEGIN
  new.last_status_change := current_timestamp;
  return new;
end;
$update_last_status_date$ LANGUAGE plpgsql;

--Trigger

create trigger update_last_status before update on orders
for each row
when (new.status_id is distinct from old.status_id)
execute procedure update_last_status_date();

    SQL
  end
  def down
    #code
  end
end
