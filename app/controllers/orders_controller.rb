class OrdersController < ApplicationController
  before_action :authenticate_user!
  
  def index
    date_query = orders_by_date_query(params[:start_date], params[:end_date])
    search_query = orders_by_search_query(params[:search])
    @orders = Order.where(date_query).where(
     search_query).joins(:customer, :status, :address).order(id: :desc)
     if @orders.empty?
       flash[:error] = "There is no orders with that paramets"
     end
  end

  def show
    @order = Order.find(params[:id])
  end

  def new
    @order = Order.new
  end

  def create
    @order = Order.create(order_params)
    current_user.cart.cart_items.each do |item|
      product = Product.find(item.product_id)
      order_item = OrderItem.create(order_id: @order.id, item_name: product.name, price: product.price, product_id: product.id, count: item.quantity, serial_number: product.product_code)
      order_item.save
      product.update(in_stock: product.in_stock - item.quantity)
      item.destroy
    end
    flash[:success] = "Order has been created"
    redirect_to home_path
  end

  def update
    @order = Order.find(params[:id])
  	@order.update(status_params)
    @order.save
    flash[:success] = "Status has been updated"
    redirect_to order_path
  end

  private

  def status_params
    params.require(:order).permit(:status_id)
  end

  def order_params
    params.require(:order).permit(:address_id, :delivery_method, :notes).merge(last_status_change: DateTime.now, order_date: DateTime.now, expected_arrival_time: DateTime.now + 14, status_id: 1, customer_id: current_user.customer.id)
  end


  def orders_by_search_query(search)
    if search.present?
      search_string =  '%' + search + '%'
      search_query = '"customers"."first_name" ILIKE :search_string OR
      "customers"."last_name" ILIKE :search_string OR
       "statuses"."status_name" ILIKE :search_string OR
        "addresses"."street" ILIKE :search_string OR
         "addresses"."house_number" ILIKE :search_string OR
          "addresses"."apartment_number" ILIKE :search_string OR
           "addresses"."postcode" ILIKE :search_string OR
            "addresses"."city" ILIKE :search_string',
      {search_string: search_string }
      # OR :address ILIKE :search_string OR :price ILIKE :search_string OR :status ILIKE :search_string
    end
    search_query
  end

  def orders_by_date_query(start_date, end_date)
    if start_date.present? && end_date.present?
      date_query = "order_date >= :start_date AND order_date <= :end_date",
      {start_date: start_date, end_date: end_date}
    elsif start_date.blank? && end_date.present?
      date_query = "order_date <= :end_date", {end_date: end_date}
    elsif start_date.present? && end_date.blank?
      date_query =  "order_date >= :start_date", {start_date: start_date}
    end
    date_query
  end

end
