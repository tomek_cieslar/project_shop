class NewsController < ApplicationController
  before_action :authenticate_user!
  
  def index
    @news = News.all.order(last_edit_date: :desc)
  end

  def show
    @new = News.find(params[:id])
  end

  def new
    @new = News.new
  end

  def create
    @new = News.new(news_params)
    @new.save
    flash[:success] = "News has been created"
    redirect_to news_index_path
  end

  def edit
    @new = News.find(params[:id])
  end

  def update
    @new = News.find(params[:id])
    @new.update(news_edit_params)
    @new.save
    flash[:success] = "News has been updated"
    redirect_to news_index_path
  end

  private

  def news_params
    params.require(:news).permit(:content, :public, :title).merge(last_edit_date: DateTime.now, created_date: DateTime.now)
  end

  def news_edit_params
    params.require(:news).permit(:content, :public, :title).merge(last_edit_date: DateTime.now)
  end
end
