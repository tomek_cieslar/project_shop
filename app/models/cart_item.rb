class CartItem < ApplicationRecord
  belongs_to :cart

  def product(id)
    Product.find(id)
  end
end
