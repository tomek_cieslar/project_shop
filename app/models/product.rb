class Product < ApplicationRecord
  has_many :serial_numbers
  has_many :order_items
  belongs_to :manufacturer
  belongs_to :category
end
