require 'spec_helper'
require 'rails_helper'

RSpec.describe ProductsController do

  describe "GET index" do
    it "customer cant see product if filter is wrong" do
      args = ["\"categories\".\"category_name\" ILIKE :search_string OR\n      \"manufacturers\".\"manufacturer_name\" ILIKE :search_string OR\n       \"products\".\"name\" ILIKE :search_string", {search_string: "%123%"}]
      allow(controller).to receive(:products_by_search_query) {args}
      get :index
      expect(assigns(:products)).to eq([])
    end
    it "customer will see filtred products" do
      args = ["\"categories\".\"category_name\" ILIKE :search_string OR\n      \"manufacturers\".\"manufacturer_name\" ILIKE :search_string OR\n       \"products\".\"name\" ILIKE :search_string", {search_string: "%Monitor DELL%"}]
      allow(controller).to receive(:products_by_search_query) {args}
      get :index
      expect(assigns(:products)).to eq([Product.find_by(name: "Monitor DELL")])
    end
    it "customer will see all products" do
      args = ["\"categories\".\"category_name\" ILIKE :search_string OR\n      \"manufacturers\".\"manufacturer_name\" ILIKE :search_string OR\n       \"products\".\"name\" ILIKE :search_string", {search_string: "%Monitor DELL%"}]
      allow(controller).to receive(:products_by_search_query) {nil}
      get :index
      expect(assigns(:products)).to eq([Product.find_by(name: "Monitor DELL"),Product.find_by(name: "Komputer DELL"),Product.find_by(name: "Kabel Display Port")])
    end
  end
end
