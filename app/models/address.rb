class Address < ApplicationRecord
  belongs_to :customer
  has_many :orders

  def full_address
    if street.present?
      if apartment_number.present?
        city + " " + postcode + " " + street + " " + house_number + "/" + apartment_number
      else
        city + " " + postcode + " " + street + " " + house_number
      end
    else
      if apartment_number.present?
        postcode + " " + city + " " + house_number + "/" + apartment_number
      else
        postcode + " " + city + " " + house_number
      end
    end
  end
end
