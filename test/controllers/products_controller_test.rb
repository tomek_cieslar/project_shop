require 'test_helper'

class ProductsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  setup do
    @user = users(:user_tomek)
    @product_1 = products(:product_1)
    @product_2 = products(:product_2)
    @product_3 = products(:product_3)
    @sony = manufacturers(:sony)
    @computer = categories(:computer)
    sign_in @user
  end

  test "user can see all products" do
    get products_path params: { search: nil}
    assert_response :success
     assert_select "tbody tr", { count: 3 }
  end

  test "user can search products by category" do
    get products_path params: { search: "Kabel"}
    assert_response :success
      assert_select "tbody tr", { count: 1 }
      assert_select "tbody tr td", { count: 1, text: @product_3.name }
  end

  test "user can search products by manufacturer" do
    get products_path params: { search: "DELL"}
    assert_response :success
      assert_select "tbody tr", { count: 2 }
      assert_select "tbody tr td", { count: 1, text: @product_2.name }
      assert_select "tbody tr td", { count: 1, text: @product_1.name }
  end

  test "user will see empty page if he enters incorrect data" do
    get products_path params: { search: "ZDZISLAW"}
    assert_response :success
      assert_select "tbody tr", { count: 0 }
  end

  test "Creating new products after submit with button" do
    post products_path params: {  product:
    {
    name: "Tablet",
    category_id: @computer.id,
    manufacturer_id: @sony.id,
    parameters: "duzo cali",
    description: "tablet co ma duzo cali ale jest fajny",
    in_stock: 5,
    price: 599,
    package_specification: "duza paczka",
    product_code: "xyz"
    }
    }
    assert_response :redirect
  end

end
