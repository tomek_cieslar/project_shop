require 'test_helper'

class CartsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @user = users(:user_tomek)
    @customer_1 = customers(:customer_tomek)
    @cart_1 = carts(:cart_1)
    @cart_item_1 = cart_items(:cart_item_1)
  end

end
