require 'test_helper'


class UserTest < ActiveSupport::TestCase
  test "creating user" do
    user = User.new(id: 111, email: "kubcia@ep.pl", password: "dlsadasdas",created_at: DateTime.now, updated_at: DateTime.now, access_level: 1, sign_in_count: 0)
    assert user.save
  end

  test "when customer try to create account using existing email in data base" do
    user = User.new(id: 123, email: "marus@ep.pl", password: "dlsadasdas",created_at: DateTime.now, updated_at: DateTime.now, access_level: 3, sign_in_count: 0)
    assert user.save
    user_2 = User.new(id: 155, email: "marus@ep.pl", password: "password",created_at: DateTime.now, updated_at: DateTime.now, access_level: 3, sign_in_count: 0)
    assert !user_2.save
  end

end
