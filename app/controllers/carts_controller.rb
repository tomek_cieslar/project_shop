class CartsController < ApplicationController
  # to show cart user must be logged in
  before_action :authenticate_user!
  def show
    @cart = current_user.cart
  end

  def create
    @cart = Cart.create(user_id: current_user(:id))
    @cart.save
  end

  def destroy
    current_user.cart.cart_items.each do |item|
      item.destroy
    end
    redirect_to cart_path
  end
end
