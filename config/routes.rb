Rails.application.routes.draw do
  resources :fish
  resources :carts
  resources :cart_items do
    put :add, on: :collection
    put :subb, on: :collection
  end
  devise_for :users
  resources :users
  resources :orders
  resources :news
  resources :addresses
  resources :categories do
    resources :products
  end
  resource :cart
  resources :products
  get "home", to: "home#index"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: "home#index"
end
