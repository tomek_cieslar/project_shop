require 'test_helper'

class OrderTest < ActiveSupport::TestCase
  setup do
    @user_1 = users(:user_tomek)
    @customer_1 = customers(:customer_tomek)
    @address_1 = addresses(:address_1)
    @status_1 = statuses(:status_1)
    @status_2 = statuses(:status_2)
    @order_1 = orders(:order_1)
  end

  test "when system creating order with vaild arguments" do
    order = Order.new(id: 17, customer_id: @customer_1.id, status_id: @status_1.id , last_status_change: "2018-12-20", order_date: "2018-12-20", address_id: @address_1.id, delivery_method: "DHL", expected_arrival_time: "2018-11-29")
    assert order.save
  end

  test "when system creating invalid arguments" do
    order = Order.new(id: 17, status_id: @status_1.id , last_status_change: "2018-12-20", order_date: "2018-12-20", address_id: @address_1.id, delivery_method: "DHL", expected_arrival_time: "2018-11-29")
    assert !order.save
  end

  test "Updating status in order" do
      @order_1.update(status_id: @status_2.id )
    assert @order_1.status_id == @status_2.id
  end

end
