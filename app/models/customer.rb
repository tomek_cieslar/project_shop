class Customer < ApplicationRecord
  belongs_to :user
  has_many :orders
  has_many :addresses

  def name
    first_name + " " + last_name
  end
end
