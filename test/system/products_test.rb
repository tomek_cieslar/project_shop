require "application_system_test_case"


class ProductsTest < ApplicationSystemTestCase
  include Devise::Test::IntegrationHelpers

  setup do
    @user = users(:user_marek)
    @customer_1 = customers(:customer_tomek)
    @address_1 = addresses(:address_1)
    @status_1 = statuses(:status_1)
    @status_2 = statuses(:status_2)
    @order = orders(:order_1)
    sign_in @user
  end

  test "visiting products page" do
    visit products_path
    assert_selector "h1", text: "Products"
  end

  test "when user want to search products " do
    visit products_path
    fill_in('search', :with => 'DELL')
    click_button('Search')
    assert_selector "h1", text: "Products"
    assert_selector 'tbody tr', count: 2
  end

  test "when admin want to create new product" do
    visit new_product_path
    fill_in('product[name]', :with => 'iPhone')
    select('Telefon', from: 'product[category_id]')
    fill_in('product[parameters]', :with => '64gb')
    fill_in('product[description]', :with => 'fajny telefon')
    fill_in('product[in_stock]', :with => '3')
    fill_in('product[price]', :with => '33')
    fill_in('product[package_specification]', :with => '2kg')
    fill_in('product[product_code]', :with => 'iP123')
    click_button('Create Product')
    visit products_path
    assert_selector "h1", text: "Products"
    assert_selector 'tbody tr', count: 4
  end

  test "checking if products page has content " do
    visit products_path
    assert_selector "h1", text: "Products"
    assert_selector 'tbody tr', count: 3
  end

  test "adding to cart" do
    visit products_path
    click_link('Show Product', match: :first)
    fill_in('cart_item[quantity]', with: '3')
    click_button('Add to Cart')
    click_link('MY CART')
    assert_selector "h1", text: "Cart"
    assert_selector "tbody tr", { count: 1, text: 'Komputer DELL' }
  end

end
