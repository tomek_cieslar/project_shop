class AddressesController < ApplicationController
  before_action :authenticate_user!
  def new
    @address = Address.new
  end

  def create
    @address = Address.new(address_params)
    @address.save
    flash[:success] = "News has been created"
    redirect_to new_order_path
  end

  def edit
    @address = Address.find(params[:id])
  end

  def update
    @address = Address.find(params[:id])
    @address.update(address_params)
    @address.save
    flash[:success] = "Address has been updated"
    redirect_to user_path(current_user)
  end

  private

  def address_params
    params.require(:address).permit(:street, :house_number, :apartment_number, :postcode, :city).merge(customer_id: current_user.customer.id)
  end

end
