require 'spec_helper'
require 'rails_helper'

RSpec.describe OrdersController do

  describe "GET index" do
    it "recive orders" do
      date_args = ["order_date >= :start_date AND order_date <= :end_date", {start_date: "2018-12-01", end_date: "2019-01-31"}]
      args = ["\"customers\".\"first_name\" ILIKE :search_string OR\n      \"customers\".\"last_name\" ILIKE :search_string OR\n       \"statuses\".\"status_name\" ILIKE :search_string OR\n        \"addresses\".\"street\" ILIKE :search_string OR\n         \"addresses\".\"house_number\" ILIKE :search_string OR\n          \"addresses\".\"apartment_number\" ILIKE :search_string OR\n           \"addresses\".\"postcode\" ILIKE :search_string OR\n            \"addresses\".\"city\" ILIKE :search_string", {search_string: "%Tomek%"}]
      allow(controller).to receive(:orders_by_search_query) {nil}
      allow(controller).to receive(:orders_by_date_query) {nil}
      get :index
      expect(assigns(:orders)).to eq([])
    end
  end
end
