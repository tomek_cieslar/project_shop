require 'test_helper'

class AddressTest < ActiveSupport::TestCase
  setup do
    @address_1 = addresses(:address_1)
    @address_2 = addresses(:address_2)
    @address_3 = addresses(:address_3)
    @address_4 = addresses(:address_4)
  end

  test "generating full address" do
    address = @address_1.full_address
    assert address == "Bielsko Biala 43-300 Dluga 2/1"
  end

  test "generating complete address without street nad apartment number" do
    address = @address_2.full_address
    assert address == "53-623 Dziegielow 6"
  end

  test "generating complete address without apartment number" do
    address = @address_3.full_address
    assert address == "Wroclaw 53-623 Dluga 6"
  end

  test "generating complete address without street" do
    address = @address_4.full_address
    assert address == "43-350 Dziegielow 2/1"
  end
end
