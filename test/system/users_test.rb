require "application_system_test_case"

class UsersTest < ApplicationSystemTestCase
  include Devise::Test::IntegrationHelpers

  setup do
    @user =  User.create(email: 'lolo@wp.pl', password: '123123', access_level: 3, sign_in_count: 0,created_at: '2018-12-10',  updated_at: '2018-12-10')
  end

  test "when user want to sign in" do
    visit '/'
    click_link('Login')
    fill_in('user[email]', with: 'lolo@wp.pl')
    fill_in('user[password]', with: '123123')
    click_button('Log in')
    assert_selector "h1", text: 'Home'
    assert has_link?('Logout')
  end

  test "when method_or_case" do
    sign_in @user
    visit'/'
    click_link('Logout')
    assert has_link?('Login')
  end

end
