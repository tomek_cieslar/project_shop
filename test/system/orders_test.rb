require "application_system_test_case"


class OrdersTest < ApplicationSystemTestCase
  include Devise::Test::IntegrationHelpers

  setup do
    @user = users(:user_tomek)
    @customer_1 = customers(:customer_tomek)
    @customer_2 = customers(:customer_kuba)
    @address_1 = addresses(:address_1)
    @status_1 = statuses(:status_1)
    @status_2 = statuses(:status_2)
    @order = orders(:order_1)
    sign_in @user
  end

  test "when method_or_case" do
    visit orders_path
    assert_selector "h1", text: "Orders"
    assert_selector 'tbody tr', count: 0
  end



end
