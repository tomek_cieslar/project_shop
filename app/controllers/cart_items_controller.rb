class CartItemsController < ApplicationController

  def create
    #check if user cart exist? if not create
    unless current_user.cart
      current_user.create_cart
    end
    # assign product_id to cart item
    item = current_user.cart.cart_items.find_by(product_id: params[:cart_item][:product_id])
    #if cart item exist, update quantity if not create new cart item in current user cart
    if item
      item.update(quantity: item.quantity + params[:cart_item][:quantity].to_i)
    else
      current_user.cart.cart_items.create(product_id: params[:cart_item][:product_id], quantity: params[:cart_item][:quantity])
    end
    redirect_to product_path(params[:cart_item][:product_id])
  end

  def add
    item = current_user.cart.cart_items.find_by(product_id: params[:product_id])
    item.update(quantity: item.quantity + 1)
    redirect_to cart_path(current_user.cart.id)
  end

  def subb
    item = current_user.cart.cart_items.find_by(product_id: params[:product_id])
    if item
        if item.quantity > 1
          item.update(quantity: item.quantity - 1)
        else
          #if quantity == 1 remove cart item
          item.destroy
        end
    end
    redirect_to cart_path(current_user.cart.id)
  end
end
