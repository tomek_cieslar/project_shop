require "application_system_test_case"


class NewsTest < ApplicationSystemTestCase
  include Devise::Test::IntegrationHelpers
  setup do
    @user = users(:user_marek)
    sign_in @user
  end

  test "when admin create news" do
    visit '/'
    click_link('News')
    click_link('New')
    fill_in('news[title]', with: 'Tytul')
    fill_in('news[content]', with: 'tekst')
    check('news[public]')
    click_button('Create News')
    assert_selector 'tbody tr',  { count: 1, text: 'Tytul' }
  end


    test "when admin edit news" do
      visit '/'
      click_link('News')
      click_link('New')
      fill_in('news[title]', with: 'Tytul')
      fill_in('news[content]', with: 'tekst')
      click_button('Create News')
      click_link('Edit')
      check('news[public]')
      click_button('Update News')
      visit'/'
      assert_selector 'ul'

    end

    test "when admin create unpublic news" do
      visit '/'
      click_link('News')
      click_link('New')
      fill_in('news[title]', with: 'Tytul')
      fill_in('news[content]', with: 'tekst')
      click_button('Create News')
      visit'/'
      !assert_selector 'ul'

    end
end
