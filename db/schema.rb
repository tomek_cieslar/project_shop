# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_12_10_134814) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", id: :serial, force: :cascade do |t|
    t.integer "customer_id", null: false
    t.string "street", limit: 30
    t.string "house_number", limit: 10, null: false
    t.string "apartment_number", limit: 10
    t.string "postcode", limit: 6, null: false
    t.string "city", limit: 30, null: false
    t.index ["id"], name: "address_id_idx", unique: true
  end

  create_table "cart_items", id: :serial, force: :cascade do |t|
    t.integer "cart_id", null: false
    t.integer "product_id", null: false
    t.integer "quantity", null: false
    t.index ["id"], name: "cart_item_id_idx", unique: true
  end

  create_table "carts", id: :serial, force: :cascade do |t|
    t.integer "user_id", null: false
    t.index ["id"], name: "cart_id_idx", unique: true
  end

  create_table "categories", id: :serial, force: :cascade do |t|
    t.string "category_name", limit: 20, null: false
    t.index ["category_name"], name: "categories_category_name_key", unique: true
    t.index ["id"], name: "category_id_idx", unique: true
  end

  create_table "customers", id: :serial, force: :cascade do |t|
    t.integer "user_id", null: false
    t.string "first_name", limit: 20, null: false
    t.string "last_name", limit: 30, null: false
    t.index ["id"], name: "customer_id_idx", unique: true
    t.index ["user_id"], name: "customers_user_id_key", unique: true
  end

  create_table "manufacturers", id: :serial, force: :cascade do |t|
    t.string "manufacturer_name", limit: 20, null: false
    t.index ["id"], name: "manufacturer_id_idx", unique: true
    t.index ["manufacturer_name"], name: "manufacturers_manufacturer_name_key", unique: true
  end

  create_table "news", id: :serial, force: :cascade do |t|
    t.datetime "created_date", null: false
    t.datetime "last_edit_date", null: false
    t.string "title", limit: 200, null: false
    t.string "content", limit: 1000, null: false
    t.json "thumbnail"
    t.string "link", limit: 200
    t.boolean "public", null: false
    t.index ["id"], name: "news_id_idx", unique: true
  end

  create_table "order_items", id: :serial, force: :cascade do |t|
    t.integer "order_id", null: false
    t.string "item_name", limit: 50, null: false
    t.decimal "price", precision: 7, scale: 2, null: false
    t.integer "product_id"
    t.integer "count", null: false
    t.string "serial_number", limit: 16
    t.index ["id"], name: "order_item_id_idx", unique: true
  end

  create_table "orders", id: :serial, force: :cascade do |t|
    t.integer "customer_id", null: false
    t.integer "status_id", null: false
    t.datetime "last_status_change", null: false
    t.date "order_date", null: false
    t.integer "address_id", null: false
    t.string "delivery_method", limit: 30, null: false
    t.date "expected_arrival_time", null: false
    t.string "notes", limit: 1000
    t.index ["id"], name: "order_id_idx", unique: true
  end

  create_table "products", id: :serial, force: :cascade do |t|
    t.string "name", limit: 50, null: false
    t.integer "category_id", null: false
    t.integer "manufacturer_id", null: false
    t.string "parameters", limit: 500
    t.text "description", null: false
    t.json "thumbnail"
    t.integer "in_stock", null: false
    t.decimal "price", precision: 7, scale: 2, null: false
    t.string "package_specification", limit: 20, null: false
    t.string "product_code", limit: 20, null: false
    t.index ["id"], name: "product_id_idx", unique: true
    t.index ["product_code"], name: "products_product_code_key", unique: true
  end

  create_table "serial_numbers", id: :serial, force: :cascade do |t|
    t.integer "product_id", null: false
    t.string "serial_number", limit: 16, null: false
    t.index ["id"], name: "serial_number_item_id_idx", unique: true
  end

  create_table "statuses", id: :serial, force: :cascade do |t|
    t.string "status_name", limit: 20, null: false
    t.index ["id"], name: "status_id_idx", unique: true
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "email", limit: 30, null: false
    t.string "encrypted_password", limit: 64, null: false
    t.integer "access_level", default: 3, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "reset_password_token", limit: 64
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.index ["email"], name: "user_email_idx", unique: true
    t.index ["email"], name: "users_email_key", unique: true
    t.index ["id"], name: "user_id_idx", unique: true
    t.index ["reset_password_token"], name: "user_reset_password_token_idx", unique: true
  end

  add_foreign_key "addresses", "customers", name: "addresses_customer_id_fkey", on_delete: :restrict
  add_foreign_key "cart_items", "carts", name: "cart_items_cart_id_fkey", on_delete: :restrict
  add_foreign_key "cart_items", "products", name: "cart_items_product_id_fkey", on_delete: :restrict
  add_foreign_key "carts", "users", name: "carts_user_id_fkey", on_delete: :restrict
  add_foreign_key "customers", "users", name: "customers_user_id_fkey", on_delete: :restrict
  add_foreign_key "order_items", "orders", name: "order_items_order_id_fkey", on_delete: :restrict
  add_foreign_key "order_items", "products", name: "order_items_product_id_fkey", on_delete: :restrict
  add_foreign_key "orders", "addresses", name: "orders_address_id_fkey", on_delete: :restrict
  add_foreign_key "orders", "customers", name: "orders_customer_id_fkey", on_delete: :restrict
  add_foreign_key "orders", "statuses", name: "orders_status_id_fkey", on_delete: :restrict
  add_foreign_key "products", "categories", name: "products_category_id_fkey", on_delete: :restrict
  add_foreign_key "products", "manufacturers", name: "products_manufacturer_id_fkey", on_delete: :restrict
  add_foreign_key "serial_numbers", "products", name: "serial_numbers_product_id_fkey", on_delete: :restrict
end
