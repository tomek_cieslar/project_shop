class OrderItem < ApplicationRecord
  belongs_to :order
  belongs_to :product

  def count_price
    price * count
  end
end
