require 'test_helper'
class OrderItemTest < ActiveSupport::TestCase
  setup do
    @order_item_1 = order_items(:order_item_1)
  end

  test "counting price" do
    price = @order_item_1.count_price
    #in db price like 5,99 is placed as integeer 599
    assert price == 5000
  end
end
