require 'test_helper'

class OrdersControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @user = users(:user_tomek)
    @customer_1 = customers(:customer_tomek)
    @address_1 = addresses(:address_1)
    @status_1 = statuses(:status_1)
    @status_2 = statuses(:status_2)
    @order = orders(:order_1)
  end

  test "can not see order if U not logged in" do
    get orders_path params: {search: nil, end_date: nil, start_date: nil}
    assert_response :redirect
  end

end
