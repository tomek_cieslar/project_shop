require 'test_helper'

class CartItemsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  setup do
    @user = users(:user_tomek)
    @cart_1 = carts(:cart_1)
    @cart_item_1 = cart_items(:cart_item_1)
    sign_in @user
  end


  test "adding to cart" do
    put add_cart_items_path(params: {product_id: 11})
    assert   @user.cart.cart_items.first.quantity == 3
  end

  test "subtrackt from cart" do
    put subb_cart_items_path(params: {product_id: 11})
    assert   @user.cart.cart_items.first.quantity == 1
  end

  test "remove item form cart" do
    put subb_cart_items_path(params: {product_id: 11})
    put subb_cart_items_path(params: {product_id: 11})
    assert   @user.cart.cart_items.empty?
  end
end
