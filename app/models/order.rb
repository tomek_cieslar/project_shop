class Order < ApplicationRecord
  has_many :order_items
  belongs_to :address
  belongs_to :customer
  belongs_to :status


end
